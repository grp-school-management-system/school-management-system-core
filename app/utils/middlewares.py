from loguru import logger
from fastapi import Request, HTTPException, status
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.responses import Response
from typing import Callable, Awaitable
import sys


class ExceptionHandlerMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self, request: Request, call_next: Callable[[Request], Awaitable[Response]]
    ) -> Response:
        try:
            return await call_next(request)
        except HTTPException as http_exception:
            return JSONResponse(
                status_code=http_exception.status_code,
                content={
                    "error": "client error",
                    "message": str(http_exception.detail),
                },
            )
        except Exception as ex:
            ex_type, ex_value, ex_traceback = sys.exc_info()
            logger.error(f"{ex_type} \n {ex_value} \n {ex_traceback.tb_frame}")
            return JSONResponse(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                content={"error": ex.__class__.__name__, "message": ex.args},
            )
