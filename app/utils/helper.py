from uuid import uuid4


def copy_attributes(obj: object, update_dict: dict) -> None:
    for key, value in update_dict.items():
        setattr(obj, key, value)


def get_unique_id() -> str:
    return str(uuid4())
