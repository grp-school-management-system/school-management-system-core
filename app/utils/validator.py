class ValidationException(Exception):
    def __init__(self, msg: str):
        self.message = msg


class EmailValidationException(ValidationException):
    def __init__(self, msg: str):
        super().__init__(msg)


class ContactNumberValidationException(ValidationException):
    def __init__(self, msg: str):
        super().__init__(msg)


def validate_email(email: str) -> str:
    if email is None:
        raise EmailValidationException()
    return email


def validate_contact_number(contact_number: str) -> str:
    if contact_number is None:
        raise ContactNumberValidationException()
    return contact_number
