from fastapi.exceptions import HTTPException
from fastapi import status


class SchoolDirectorNotFoundException(Exception):
    def __init__(self, message="School director not found"):
        super().__init__(message)


class UserAlreadyExistException(Exception):
    def __init__(self, message="User Already exists"):
        super().__init__(message)


class UserNotFoundException(Exception):
    def __init__(self, message="User does not exist"):
        super().__init__(message)


class InternalServerError(Exception):
    def __init__(self, message="User does not exist"):
        super().__init__(message)


class UserCreationFailedException(Exception):
    def __init__(self, message="Could not create User"):
        super().__init__(message)


class SchoolIdDoesNotExistsException(Exception):
    def __init__(self, message="School does not exists"):
        super().__init__(message)


class SchoolIdNotProvidedException(Exception):
    def __init__(self, message="School id for user not provided"):
        super().__init__(message)


class QuestionDoesNotExistsException(Exception):
    def __init__(self, message):
        super().__init__(message)


class InvalidDateFormatException(Exception):
    def __init__(self, message="could not parse date, expected format: [yyyy-mm-dd]"):
        super().__init__(message)


class InvalidTimeFormatException(Exception):
    def __init__(self, message="could not parse date, expected format: [hh-mm-ss]"):
        super().__init__(message)


class NoDetailFoundException(Exception):
    def __init__(self, message="could not find any details"):
        super().__init__(message)


class UserDoesNotBelongToSchoolException(Exception):
    def __init__(self, message):
        super().__init__(message)


class StudentAnswerNotFoundException(Exception):
    def __init__(self, message):
        super().__init__(message)


class SchoolIdNotFoundException(Exception):
    def __init__(self, message):
        super().__init__(message)


class PasswordNotMatchedException(HTTPException):
    def __init__(self, details="Incorrect username or password"):
        super().__init__(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=details,
            headers={"WWW-Authenticate": "Bearer"},
        )


class InvalidTokenException(HTTPException):
    def __init__(self):
        super().__init__(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
