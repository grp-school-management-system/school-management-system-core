from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import (
    StringSerializer,
    SerializationContext,
    MessageField,
)
from confluent_kafka import Producer

from uuid import uuid4
import os
import argparse


class User(object):
    def __init__(self, name, address, favorite_number, favorite_color):
        self.name = name
        self.favorite_color = favorite_color
        self.favorite_number = favorite_number
        self._address = address


def user_to_dict(user, ctx):
    return dict(
        name=user.name,
        favorite_number=user.favorite_number,
        favorite_color=user.favorite_color,
    )


def delivery_report(err, msg):
    if err is not None:
        print("Delivery failed for User record {}: {}".format(msg.key(), err))
        return
    print(
        "User record {} successfully produced to {} [{}] at offset {}".format(
            msg.key(), msg.topic(), msg.partition(), msg.offset()
        )
    )


def main(args):
    topic = args.topic
    schema = "user.avsc"
    path = os.path.realpath(os.path.dirname(__file__))
    with open(f"{path}/avro/{schema}") as f:
        schema_str = f.read()

    schema_registry_conf = {"url": args.schema_registry}
    schema_registry_client = SchemaRegistryClient(schema_registry_conf)

    avro_serializer = AvroSerializer(schema_registry_client, schema_str, user_to_dict)

    string_serializer = StringSerializer("utf-8")

    producer_conf = {"bootstrap.server": args.bootstrap_servers}
    producer = Producer(producer_conf)

    while True:
        producer.poll(0.0)
        try:
            user_name = input("Enter name")
            user_address = input("Address")
            user_favorite_number = int(input("Favorite Number"))
            user_favorite_color = input("Favorite Color")
            user = User(
                name=user_name,
                address=user_address,
                favorite_color=user_favorite_color,
                favorite_number=user_favorite_number,
            )
            producer.produce(
                topic=topic,
                key=string_serializer(str(uuid4())),
                value=avro_serializer(
                    user, SerializationContext(topic, MessageField.VALUE)
                ),
                on_delivery=delivery_report,
            )
        except KeyboardInterrupt:
            break
        except ValueError:
            print("Invalid input")
            continue
    print("\nflusing records..")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="AvroSerializer example")
    parser.add_argument(
        "-b",
        dest="bootstrap_servers",
        required=True,
        help="Bootstrap broker(s) (host[:port])",
    )
    parser.add_argument(
        "-s",
        dest="schema_registry",
        required=True,
        help="Schema Registry (http(s)://host[:port]",
    )
    parser.add_argument(
        "-t", dest="topic", default="example_serde_avro", help="Topic name"
    )
    parser.add_argument(
        "-p", dest="specific", default="true", help="Avro specific record"
    )

    main(parser.parse_args())
