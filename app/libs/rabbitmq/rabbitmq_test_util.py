from app.libs.rabbitmq_util import QueueClient
from typing import List, Any
import time
from multiprocessing import Process


def test_sender():
    publisher = QueueClient()
    print("calling test published")
    for i in range(10):
        print(f"index: {i}")
        publisher.json_publish(
            queue_name="queue_test", body={"hello": "world", "index": i}
        )
        time.sleep(1)


def callback(events: List[Any]) -> None:
    print("starting consuming: ")
    for event in events:
        print(event)


def test_receiver():
    consumer = QueueClient()
    print("calling test receiver")
    consumer.start_json_consumer(
        queue_name="queue_test", callback=callback, batch_size=5
    )


def test_rabbitmq():
    p1 = Process(target=test_sender)
    p2 = Process(target=test_receiver)
    p1.start()
    p2.start()
    p1.join()
    p2.join()
