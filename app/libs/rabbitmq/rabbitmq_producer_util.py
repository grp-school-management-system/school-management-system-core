# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205

import logging
import pika
from pika import DeliveryMode
from pika.exchange_type import ExchangeType

# logging.basicConfig(level=logging.DEBUG)

credentials = pika.PlainCredentials("khritik", "my-secret-password")
parameters = pika.ConnectionParameters(
    host="localhost", port=5672, credentials=credentials
)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


channel.queue_declare(queue="task_queue", durable=True)
for i in range(10, 20):
    message = f"message: {i}"
    channel.basic_publish(
        exchange="",
        routing_key="task_queue",
        body=message,
        properties=pika.BasicProperties(delivery_mode=pika.DeliveryMode.Persistent),
    )
    print(f" [x] Sent {message}")
# connection.close()


# channel.exchange_declare(exchange="test_exchange",
#                          exchange_type=ExchangeType.direct,
#                          passive=False,
#                          durable=True,
#                          auto_delete=False)

# print("Sending message to create a queue")
# channel.basic_publish(
#     'test_exchange', 'standard_key', 'queue:group',
#     pika.BasicProperties(content_type='text/plain',
#                          delivery_mode=DeliveryMode.Transient))

# connection.sleep(5)

# print("Sending text message to group")
# channel.basic_publish(
#     'test_exchange', 'group_key', 'Message to group_key',
#     pika.BasicProperties(content_type='text/plain',
#                          delivery_mode=DeliveryMode.Transient))

# connection.sleep(5)

# print("Sending text message")
# channel.basic_publish(
#     'test_exchange', 'standard_key', 'Message to standard_key',
#     pika.BasicProperties(content_type='text/plain',
#                          delivery_mode=DeliveryMode.Transient))

# connection.close()
