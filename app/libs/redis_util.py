import redis
from app.config.settings import get_settings
from typing import Optional
import orjson
import re
import secrets

settings = get_settings()

# Redis accepts keys up to 512MB in size, but that much size is not needed
MAX_KEY_LENGTH = 1024


class FastAPIExperimentRedisError(Exception):
    pass


class FastAPIExperimentKeyTooLongError(FastAPIExperimentRedisError):
    pass


class FastAPIExperimentRedisOfWrongFormatError(FastAPIExperimentRedisError):
    pass


def get_redis_client() -> redis.Redis[bytes]:
    return redis.Redis(
        host=settings.REDIS_HOST,
        port=settings.REDIS_PORT,
        password=settings.REDIS_PASSWORD,
        db=0,
        decode_responses=True,
    )


def put_dict_in_redis(
    redis_client: redis.Redis[bytes],
    key_format: str,
    data_to_store,
    expiration_seconds: int,
    token_length: int = 64,
    token: Optional[str] = None,
):
    key_length = len(key_format) - len("{token}") + token_length
    if key_length > MAX_KEY_LENGTH:
        raise FastAPIExperimentKeyTooLongError(
            f"Requested key too long in put_dict_in_redis. Key format: {key_format}, token length: {token_length}",
        )
    if token is None:
        token = secrets.token_hex(token_length // 2)
    key = key_format.format(token=token)
    with redis_client.pipeline() as pipeline:
        pipeline.set(key, orjson.dumps(data_to_store))
        pipeline.expire(key, expiration_seconds)
        pipeline.execute()
    return key


def get_dict_from_redis(redis_client: redis.Redis[bytes], key_format: str, key: str):
    if len(key) > MAX_KEY_LENGTH:
        raise FastAPIExperimentKeyTooLongError(
            f"Requested key too long in get_dict_from_redis: {key}"
        )

    validate_key_fits_format(key, key_format)
    data = redis_client.get(key)
    if data is None:
        return None
    return orjson.loads(data)


def validate_key_fits_format(key: str, key_format: str) -> None:
    assert "{token}" in key_format
    regex = key_format.format(token=r"[a-zA-Z0-9]+")

    if not re.fullmatch(regex, key):
        raise FastAPIExperimentRedisOfWrongFormatError(
            f"{key} does not match format {key_format}"
        )
