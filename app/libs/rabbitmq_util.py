import pika
import time
import orjson
import logging
import ssl

from app.config.settings import get_settings
from typing import Set, Optional, TypeVar, Callable, Mapping, Any, List, Dict
from pika.channel import Channel
from pika.adapters.blocking_connection import BlockingChannel
from pika.spec import Basic
from typing_extensions import TypeAlias
from collections import defaultdict

settings = get_settings()
ChannelT = TypeVar("ChannelT", Channel, BlockingChannel)
Consumer: TypeAlias = Callable[
    [ChannelT, Basic.Deliver, pika.BasicProperties, bytes], None
]


class QueueClient:
    def __init__(self, prefetch: int = 0):
        self.channel = None
        self.log = logging.getLogger("QueueClient")
        self.queues: Set[str] = set()
        self.prefetch = prefetch
        self.connection: Optional[pika.BlockingConnection] = None
        self.consumers: Dict[str, Set[Consumer[ChannelT]]] = defaultdict(set)
        self.is_consuming = False

    def _get_parameters(self):
        credentials = pika.PlainCredentials(
            settings.RABBITMQ_USERNAME, settings.RABBITMQ_PASSWORD
        )

        return pika.ConnectionParameters(
            settings.RABBITMQ_HOST,
            port=settings.RABBITMQ_PORT,
            credentials=credentials,
        )

    def _connect(self):
        start = time.time()
        self.connection = pika.BlockingConnection(self._get_parameters())
        self.channel = self.connection.channel()
        self.channel.basic_qos(prefetch_count=self.prefetch)
        self.log.info(
            "SimpleQueueClient connected (connecting took %.3fs)", time.time() - start
        )

    def _reconnect(self):
        self.connection = None
        self.channel = None
        self.queues = set()
        self._connect()

    def close(self) -> None:
        if self.connection is not None:
            self.connection.close()

    def ensure_queue(
        self, queue_name: str, callback: Callable[[BlockingChannel], object]
    ) -> None:
        if self.connection is None or not self.connection.is_open:
            self._connect()
            assert self.channel is not None
        else:
            assert self.channel is not None

        if queue_name not in self.queues:
            self.channel.queue_declare(queue=queue_name, durable=True)
            self.queues.add(queue_name)
        callback(self.channel)

    def publish(self, queue_name: str, body: bytes) -> None:
        def do_publish(channel: ChannelT) -> None:
            self.channel.basic_publish(
                exchange="",
                routing_key=queue_name,
                body=body,
                properties=pika.BasicProperties(delivery_mode=2),
            )

        self.ensure_queue(queue_name, do_publish)

    def json_publish(self, queue_name: str, body: Mapping[str, Any]) -> None:
        data = orjson.dumps(body)
        try:
            self.publish(queue_name, data)
            return
        except pika.exceptions.AMQPConnectionError:
            self.log.warning(
                "Failed to send to rabbitmq, trying to reconnect and send again"
            )
        self._reconnect()
        self.publish(queue_name, data)

    def start_json_consumer(
        self,
        queue_name: str,
        callback: Callable[[List[Dict[str, Any]]], None],
        batch_size: int = 1,
        timeout: Optional[int] = None,
    ) -> None:
        if batch_size == 1:
            timeout = None

        def do_consume(channel: BlockingChannel) -> None:
            events: List[Dict[str, Any]] = []
            last_process = time.time()
            max_processed: Optional[int] = None
            self.is_consuming = True
            for method, properties, body in channel.consume(
                queue_name, inactivity_timeout=timeout
            ):
                if body is not None:
                    assert method is not None
                    events.append(orjson.loads(body))
                    max_processed = method.delivery_tag
                now = time.time()
                if len(events) >= batch_size or (
                    timeout and now >= last_process + timeout
                ):
                    if events:
                        assert max_processed is not None
                        try:
                            callback(events)
                            channel.basic_ack(max_processed, multiple=True)
                        except BaseException:
                            if channel.is_open:
                                channel.basic_nack(max_processed, multiple=True)
                            raise
                        events = []
                    last_process = now
                if not self.is_consuming:
                    break

        self.ensure_queue(queue_name, do_consume)

    def local_queue_size(self) -> int:
        assert self.channel is not None
        return self.channel.get_waiting_message_count() + len(
            self.channel._pending_events
        )

    def stop_consuming(self) -> None:
        assert self.channel is not None
        assert self.is_consuming
        self.is_consuming = False
        self.channel.stop_consuming()
