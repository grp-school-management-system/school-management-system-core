from elasticsearch import Elasticsearch
from typing import Any, List

ELASTICSEARCH_ENDPOINT = "https://localhost:9200"
ELASTICSEARCH_USER = "elastic"
ELASTICSEARCH_PASSWORD = "0fOK2145X20SwNMox8wG2O9E"
ELASTICSEARCH_TLS_KEY = "./tls.crt"


class ElasticsearchClient:
    def __init__(self):
        self.client = Elasticsearch(
            hosts=ELASTICSEARCH_ENDPOINT,
            basic_auth=(ELASTICSEARCH_USER, ELASTICSEARCH_PASSWORD),
            ca_certs=ELASTICSEARCH_TLS_KEY,
            request_timeout=10,
        )

    def create_index(self, index_name: str) -> Any:
        return self.client.indices.create(index_name)

    def insert_document(self, index_name: str, document_id: str, document: Any) -> Any:
        return self.client.index(index=index_name, id=document_id, document=document)

    def fetch_document(self, index_name: str, document_id: str) -> Any:
        return self.client.get(index=index_name, id=document_id)

    def search_document(self, index_name: str, query: Any) -> Any:
        return self.client.search(index=index_name, query=query)

    def update_document(self, index_name: str, document_id: str, document: Any) -> None:
        return self.client.update(index=index_name, id=document_id, doc=document)

    def delete_document(self, index_name: str, document_d: str) -> Any:
        return self.client.delete(index=index_name, id=document_d)

    def delete_index(self, index_name: str):
        return self.client.indices.delete(index=index_name)
