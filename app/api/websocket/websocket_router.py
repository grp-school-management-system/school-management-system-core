from fastapi import APIRouter, WebSocket
from app.api.websocket.users import ws_users_router


websocket_router = APIRouter()
websocket_router.include_router(ws_users_router, prefix="/ws")
