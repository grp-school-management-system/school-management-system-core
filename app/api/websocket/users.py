from fastapi import WebSocket, APIRouter, WebSocketDisconnect
from app.websocket.connection_manager import ConnectionManager

ws_users_router = APIRouter()

manager = ConnectionManager()


@ws_users_router.websocket("/users")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            await manager.send_personal_message(f"You wrote: {data}", websocket)
            await manager.broadcast(f"Client says: {data}")
    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast(f"Client left the chat")
