from fastapi import APIRouter, Depends
from typing import Any, Union, List
from sqlalchemy.orm import Session

from app.orm.db_connection import get_db
from app.service import leaves_service

leaves_router = APIRouter()
