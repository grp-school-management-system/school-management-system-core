from fastapi import APIRouter, Depends, Query, WebSocket
from typing import Any, Union, List, Annotated
from sqlalchemy.orm import Session

from app.orm.db_connection import get_db
from app.models import user_schemas
from app.service import user_service
from app.common.auth.authorization import authorization
from loguru import logger


users_router = APIRouter()


@users_router.get("/")
def get_user_data(
    user_ids: Annotated[List[str], Query()],
    logged_in_user: Annotated[user_schemas.LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> List[user_schemas.User]:
    return user_service.get_users_by_ids(db, user_ids)


@users_router.get("/me")
def validate_token(
    logged_in_user: Annotated[user_schemas.LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> user_schemas.BaseUserDTO:
    return user_service.get_current_user(logged_in_user, db)


@users_router.get("/{user_id}")
def get_user_by_id(
    user_id: str,
    logged_in_user: Annotated[user_schemas.LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> user_schemas.User:
    return user_service.get_user_by_id(db, user_id)


@users_router.post("/login")
def login_user(
    login_payload: user_schemas.LoginUserDTO, db: Session = Depends(get_db)
) -> user_schemas.LoginResponseDTO:
    return user_service.login_user(db, login_payload)


@users_router.post("/signup")
def create_user(
    user: user_schemas.CreateUserDTO, db: Session = Depends(get_db)
) -> user_schemas.BaseUserDTO:
    logger.info(f"Request to create new user {user}")
    return user_service.create_user(db, user)


@users_router.post("/search_by", response_model=Union[user_schemas.User, None])
def search_user_by(
    user: user_schemas.User,
    logged_in_user: Annotated[user_schemas.LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> Union[user_schemas.User, None]:
    return user_service.search_user_by_fields_or(db, user=user)


@users_router.post("/{user_id}")
def update_user(
    user_id: str,
    logged_in_user: Annotated[user_schemas.LoggedInUser, Depends(authorization)],
    user: user_schemas.UpdateUserDTO,
    db: Session = Depends(get_db),
) -> user_schemas.User:
    return user_service.update_user(db, user_id, user)


@users_router.delete("/{user_id}")
def delete_user_by_id(user_id: str, db: Session = Depends(get_db)):
    return user_service.delete_user_by_field(db, user_id)
