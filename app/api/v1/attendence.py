from fastapi import APIRouter, Depends
from typing import Any, Union, List
from sqlalchemy.orm import Session
from app.models.attendence import BaseAttendence
from app.orm.db_connection import get_db
from app.service import attendence_service

attendence_router = APIRouter()


@attendence_router.get("/school/{school_id}/user/{user_id}")
def get_all_users_attendence(
    school_id: str, user_id: str, db: Session = Depends(get_db)
) -> List[BaseAttendence]:
    return attendence_service(db, school_id, user_id)


@attendence_router.post("/")
def create_new_attendence():
    pass
