from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.orm.db_connection import get_db
from app.models.school import CreateSchoolDTO, BaseSchool, UpdateSchoolDTO
from app.models.user_schemas import BaseUserDTO
from app.service import school_service
from typing import Any, List, Annotated
from app.models.user_schemas import LoggedInUser
from app.common.auth.authorization import authorization
from loguru import logger
import orjson

school_router = APIRouter()


@school_router.get("/all")
def get_all_school_info(
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> List[BaseSchool]:
    return school_service.get_all_school_list(db)


@school_router.get("/{school_id}")
def get_new_info_by_id(
    school_id: str,
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> BaseSchool:
    return school_service.get_school_info_by_id(db, school_id)


@school_router.get("/{school_id}/users")
def get_school_users(
    school_id: str,
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> List[BaseUserDTO]:
    return school_service.get_all_school_users(db, school_id)


@school_router.post("/")
def create_new_school(
    school_req: CreateSchoolDTO,
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> BaseSchool:
    return school_service.create_school(db, school_req)


@school_router.put("/{school_id}")
def update_school_info(
    school_id: str,
    school_info: UpdateSchoolDTO,
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> BaseSchool:
    return school_service.update_school_info(
        db, school_id=school_id, update_school_schema=school_info
    )


@school_router.delete("/{school_id}")
def delete_school_by_id(
    school_id: str,
    logged_in_user: Annotated[LoggedInUser, Depends(authorization)],
    db: Session = Depends(get_db),
) -> str:
    return school_service.delete_school_by_id(db, school_id)
