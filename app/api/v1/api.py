from fastapi import APIRouter

from app.api.v1.addresses import address_router
from app.api.v1.attendence import attendence_router
from app.api.v1.exams import exams_router
from app.api.v1.leaves import leaves_router
from app.api.v1.profiles import profile_router
from app.api.v1.questions import question_router
from app.api.v1.schools import school_router
from app.api.v1.users import users_router


api_router = APIRouter()

api_router.include_router(address_router, prefix="/address", tags=["Address"])
api_router.include_router(attendence_router, prefix="/attendence", tags=["Attendence"])
api_router.include_router(exams_router, prefix="/exams", tags=["Exams"])
api_router.include_router(leaves_router, prefix="/leaves", tags=["Leaves"])
api_router.include_router(profile_router, prefix="/profile", tags=["Profile"])
api_router.include_router(question_router, prefix="/question", tags=["Question"])
api_router.include_router(school_router, prefix="/school", tags=["school"])
api_router.include_router(users_router, prefix="/users", tags=["users"])
