from sqlalchemy import Integer, String, ForeignKey, DateTime, Enum, Boolean, Date, Time
from sqlalchemy.orm import relationship, Mapped, mapped_column
from app.orm.db_connection import Base
import enum
from enum import auto
from datetime import datetime, date, time
from typing import List


class AddressType(enum.Enum):
    DEFAULT = "DEFAULT"
    HOME = "HOME"
    SCHOOL = "SCHOOL"
    UNIVERSITY = "UNIVERSITY"
    INSTITUTE = "INSTITUTE"


class UserType(enum.Enum):
    ADMIN = "ADMIN"
    ORGANIZATION_ADMIN = "ORGANIZATION_ADMIN"
    DIRECTOR = "DIRECTOR"
    PRINCIPAL = "PRINCIPAL"
    TEACHER = "TEACHER"
    GUARDIAN = "GUARDIAN"
    PARENTS = "PARENTS"
    STUDENT = "STUDENT"
    STAFF = "STAFF"


class SchoolType(enum.Enum):
    HEAD_OFFICE = "HEAD_OFFICE"
    STATE_BRANCH_HEAD = "STATE_BRANCH_HEAD"
    BRANCH = "BRANCH"
    INDIVIDUAL = "INDIVIDUAL"


class ClassStandardName(enum.Enum):
    NURSARY = enum.auto()
    UKG = enum.auto()
    LKG = enum.auto()
    FIRST = enum.auto()
    SECOND = enum.auto()
    THRIRD = enum.auto()
    FOURTH = enum.auto()
    FIVETH = enum.auto()
    SIXTH = enum.auto()
    SEVENTH = enum.auto()
    EIGHTTH = enum.auto()
    NINETH = enum.auto()
    TENTH = enum.auto()


class ClassPeriodName(enum.Enum):
    FIRST = enum.auto()
    SECOND = enum.auto()
    THRIRD = enum.auto()
    FOURTH = enum.auto()
    FIVETH = enum.auto()
    SIXTH = enum.auto()
    SEVENTH = enum.auto()
    EIGHTTH = enum.auto()
    NINETH = enum.auto()
    TENTH = enum.auto()


class DepartmentType(enum.Enum):
    COMPUTER_SCIENCE_AND_ENGINEERING = enum.auto()
    CIVIL_ENGINEERING = auto()
    ELECTRICAL_ENGINEERING = auto()
    ELECTICAL_AND_ELECTRONICS_ENGINEERING = auto()
    MECHANICAL_ENGINEERING = auto()
    PRODUCTION_ENGINEERING = auto()
    BIO_TECHNICAL_ENGINEERING = auto()
    CHEMICAL_ENGINEERING = auto()
    MATHEMICAL = auto()
    APPLIED_PHUSICS = auto()


class SubjectName(enum.Enum):
    MATHS = enum.auto()
    SCIENCE = enum.auto()
    SOCIAL_STUDEIES = enum.auto()
    ENGLIS_LITERATURE = enum.auto()
    HINDI_LITERATURE = enum.auto()
    PHYSICS = enum.auto()
    BIOLOGY = enum.auto()
    CHEMISTRY = enum.auto()
    FINE_ARTS = enum.auto()
    PHYSICAL_EDUCATION = enum.auto()
    ARTS = enum.auto()
    SPORTS = enum.auto()


class ExamType(enum.Enum):
    UNIT_TEST = enum.auto()
    MONTHLY_TEST = enum.auto()
    SEMESTER_EXAM = enum.auto()
    ANNUAL_EXAM = enum.auto()
    BOARD_EXAM = enum.auto()
    CLASS_TEST = enum.auto()


class WekkDayName(enum.Enum):
    MONDAY = enum.auto()
    TUESDAY = enum.auto()
    WEDNESDAY = enum.auto()
    THURSDAY = enum.auto()
    FRIDAY = enum.auto()
    SATURDAY = enum.auto()
    SUNDAY = enum.auto()


STR_LEN_10 = 10
STR_LEN_50 = 50
STR_LEN_100 = 100
STR_LEN_150 = 150
STR_LEN_200 = 200
STR_LEN_250 = 250
STR_LEN_255 = 255

USER_ID_KEY_REF = "users.user_id"
CLASS_STANDARD_ID_KEY_REF = "class_standard.class_id"
SCHOOL_ID_KEY_REF = "school.school_id"
ADDRESS_ID_KEY_REF = "address.id"
EXAM_ID_KEY_REF = "exam.exam_id"
QUESTION_ID_KEY_REF = "questions_bank.question_id"
CLASS_ROUTINE_ID_KEY_REF = "class_routine.id"


class TrackMixin:
    created_on: Mapped[datetime] = mapped_column(DateTime, nullable=True)
    created_by: Mapped[str] = mapped_column(String(STR_LEN_100), nullable=True)

    updated_on: Mapped[datetime] = mapped_column(DateTime, nullable=True)
    updated_by: Mapped[str] = mapped_column(String(STR_LEN_100), nullable=True)


class School(Base, TrackMixin):
    __tablename__ = "school"

    school_id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    name: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=False)
    registration_number: Mapped[str] = mapped_column(
        String(STR_LEN_255), nullable=False
    )
    affiliation_number: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)
    school_type: Mapped[SchoolType] = mapped_column(
        Enum(SchoolType, length=STR_LEN_50, native_enum=False)
    )

    address: Mapped["Address"] = relationship(
        "Address", back_populates="school_address", cascade="all, delete, delete-orphan"
    )
    class_standards: Mapped[List["ClassStandard"]] = relationship(
        "ClassStandard", back_populates="school"
    )
    routine_achieve: Mapped[List["ClassRoutineArchive"]] = relationship(
        "ClassRoutineArchive", back_populates="school"
    )
    users: Mapped[List["User"]] = relationship("User", back_populates="school")


class User(Base, TrackMixin):
    __tablename__ = "users"

    user_id: Mapped[str] = mapped_column(String(STR_LEN_100), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    first_name: Mapped[str] = mapped_column(String(STR_LEN_50), nullable=False)
    last_name: Mapped[str] = mapped_column(String(STR_LEN_50), nullable=False)
    email: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=False, unique=True)
    primary_contact_number: Mapped[str] = mapped_column(
        String(STR_LEN_10), nullable=False
    )
    hashed_password: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=True)
    login_method: Mapped[str] = mapped_column(String(STR_LEN_50), nullable=True)
    user_type: Mapped[UserType] = mapped_column(
        Enum(UserType, length=STR_LEN_50, native_enum=False), nullable=False
    )
    current_class: Mapped[str] = mapped_column(
        Enum(ClassStandardName, length=STR_LEN_50, native_enum=False), nullable=True
    )
    registration_number: Mapped[str] = mapped_column(String(STR_LEN_100), nullable=True)
    registration_year: Mapped[datetime] = mapped_column(DateTime, nullable=True)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True)

    addresses: Mapped[List["Address"]] = relationship(
        "Address", back_populates="user", cascade="all, delete, delete-orphan"
    )

    # Class Standard
    heading_class: Mapped[List["ClassStandard"]] = relationship(
        "ClassStandard",
        foreign_keys="ClassStandard.class_teacher_id",
        back_populates="class_teacher",
    )
    monitoring_class: Mapped[List["ClassStandard"]] = relationship(
        "ClassStandard",
        foreign_keys="ClassStandard.class_monitor_id",
        back_populates="class_monitor",
    )
    teaching_routine: Mapped[List["ClassRoutine"]] = relationship(
        "ClassRoutine", back_populates="assigned_teacher"
    )
    # Student Exam Responses
    exam_answers: Mapped[List["StudentExamResponse"]] = relationship(
        "StudentExamResponse", back_populates="student"
    )
    # Class Routine Archive
    archived_teaching_routine: Mapped[List["ClassRoutineArchive"]] = relationship(
        "ClassRoutineArchive",
        foreign_keys="ClassRoutineArchive.assigned_teacher_id",
        back_populates="assigned_teacher",
    )
    archived_replacing_teching_routine: Mapped[
        List["ClassRoutineArchive"]
    ] = relationship(
        "ClassRoutineArchive",
        foreign_keys="ClassRoutineArchive.replaced_teacher_id",
        back_populates="replaced_teacher",
    )
    # Leaves
    approved_l1_leaves: Mapped[List["Leaves"]] = relationship(
        "Leaves",
        foreign_keys="Leaves.first_level_approver_id",
        back_populates="first_level_approver",
    )
    approved_l2_leaves: Mapped[List["Leaves"]] = relationship(
        "Leaves",
        foreign_keys="Leaves.second_level_approver_id",
        back_populates="second_level_approver",
    )
    # Attendence
    # attendences: Mapped[List["Attendence"]] = relationship("Attendence", back_populates="user")
    # Student Exam Marks
    exams: Mapped[List["StudentExamMarks"]] = relationship(
        "StudentExamMarks",
        foreign_keys="StudentExamMarks.student_id",
        back_populates="student",
    )
    assigned_exam: Mapped[List["StudentExamMarks"]] = relationship(
        "StudentExamMarks",
        foreign_keys="StudentExamMarks.assigned_teacher_id",
        back_populates="assigned_teacher",
    )
    invigilating_exam: Mapped[List["StudentExamMarks"]] = relationship(
        "StudentExamMarks",
        foreign_keys="StudentExamMarks.invigilator_id",
        back_populates="invigilator",
    )

    school: Mapped["School"] = relationship("School", back_populates="users")


class Address(Base, TrackMixin):
    __tablename__ = "address"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    user_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF), nullable=True
    )
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(SCHOOL_ID_KEY_REF), nullable=True
    )
    location: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=False)
    contact_number: Mapped[str] = mapped_column(String(STR_LEN_10), nullable=False)
    pin_code: Mapped[str] = mapped_column(String(STR_LEN_10), nullable=False)
    state: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=False)

    address_type: Mapped[AddressType] = mapped_column(
        Enum(AddressType, length=STR_LEN_50, native_enum=False)
    )

    user: Mapped["User"] = relationship("User", back_populates="addresses")
    school_address: Mapped["School"] = relationship("School", back_populates="address")

    def __repr__(self):
        return "<Address(id='%s', pinCode='%s', location='%s', contactNumber='%s')>" % (
            self.id,
            self.pin_code,
            self.location,
            self.contact_number,
        )


class ClassStandard(Base, TrackMixin):
    __tablename__ = "class_standard"

    class_id: Mapped[str] = mapped_column(String(STR_LEN_100), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    class_teacher_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(USER_ID_KEY_REF)
    )
    class_monitor_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(USER_ID_KEY_REF)
    )
    academic_year: Mapped[int] = mapped_column(Integer, nullable=False)
    class_standard_name: Mapped[ClassStandardName] = mapped_column(
        Enum(ClassStandardName, length=STR_LEN_50, native_enum=False)
    )

    school: Mapped["School"] = relationship("School", back_populates="class_standards")
    class_teacher: Mapped["User"] = relationship(
        "User", foreign_keys=[class_teacher_id], back_populates="heading_class"
    )
    class_monitor: Mapped["User"] = relationship(
        "User", foreign_keys=[class_monitor_id], back_populates="monitoring_class"
    )
    question_bank: Mapped[List["QuestionBank"]] = relationship(
        "QuestionBank", back_populates="class_standard"
    )
    routine_achieve: Mapped[List["ClassRoutineArchive"]] = relationship(
        "ClassRoutineArchive", back_populates="class_standard"
    )


class ClassRoutine(Base, TrackMixin):
    __tablename__ = "class_routine"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    class_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(CLASS_STANDARD_ID_KEY_REF), nullable=False
    )
    period: Mapped[ClassPeriodName] = mapped_column(
        Enum(ClassPeriodName, length=STR_LEN_50, native_enum=False)
    )
    period_length: Mapped[str] = mapped_column(String(STR_LEN_50))
    assigned_teacher_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    subject_name: Mapped[SubjectName] = mapped_column(
        Enum(SubjectName, length=STR_LEN_50, native_enum=False)
    )
    week_day: Mapped[str] = mapped_column(
        Enum(WekkDayName, length=STR_LEN_50, native_enum=False)
    )

    assigned_teacher: Mapped[User] = relationship(
        "User", back_populates="teaching_routine"
    )
    school: Mapped[School] = relationship("School")
    class_standard: Mapped[ClassStandard] = relationship("ClassStandard")


class ClassRoutineArchive(Base, TrackMixin):
    __tablename__ = "class_routine_archive"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)

    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    class_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(CLASS_STANDARD_ID_KEY_REF), nullable=False
    )
    period: Mapped[ClassPeriodName] = mapped_column(
        Enum(ClassPeriodName, length=STR_LEN_50, native_enum=False)
    )
    period_length: Mapped[time] = mapped_column(Time)
    academic_date: Mapped[date] = mapped_column(Date, default=date.today)

    assigned_teacher_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    subject_name: Mapped[str] = mapped_column(
        Enum(SubjectName, length=STR_LEN_50, native_enum=False)
    )
    teacher_attended: Mapped[bool] = mapped_column(Boolean, default=False)
    unavailability_reason: Mapped[str] = mapped_column(String(STR_LEN_255))

    replaced_teacher_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    change_in_subject: Mapped[bool] = mapped_column(Boolean)
    replaced_subject_name: Mapped[str] = mapped_column(
        Enum(SubjectName, length=STR_LEN_50, native_enum=False)
    )

    teacher_feedback: Mapped[str] = mapped_column(String(STR_LEN_255))
    extra_details: Mapped[str] = mapped_column(String(STR_LEN_255))

    school: Mapped[School] = relationship("School", back_populates="routine_achieve")
    class_standard: Mapped[ClassStandard] = relationship(
        "ClassStandard", back_populates="routine_achieve"
    )
    assigned_teacher: Mapped[User] = relationship(
        "User", foreign_keys=[assigned_teacher_id]
    )
    replaced_teacher: Mapped[User] = relationship(
        "User", foreign_keys=[replaced_teacher_id]
    )


class Leaves(Base, TrackMixin):
    __tablename__ = "leaves"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    user_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    leave_description: Mapped[str] = mapped_column(String(STR_LEN_255))
    start_date: Mapped[date] = mapped_column(Date)
    end_date: Mapped[date] = mapped_column(Date)
    first_level_approved: Mapped[bool] = mapped_column(Boolean)
    first_level_approver_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    second_level_approved: Mapped[bool] = mapped_column(Boolean)
    second_level_approver_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )

    user: Mapped[User] = relationship("User", foreign_keys=[user_id])
    school: Mapped[School] = relationship("School")
    first_level_approver: Mapped[User] = relationship(
        "User", foreign_keys=[first_level_approver_id]
    )
    second_level_approver: Mapped[User] = relationship(
        "User", foreign_keys=[second_level_approver_id]
    )


class Attendence:
    __tablename__ = "attendence"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    user_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    attendence_date: Mapped[date] = mapped_column(Date, nullable=True)
    entry_time: Mapped[time] = mapped_column(Time, nullable=True)
    exit_time: Mapped[time] = mapped_column(Time, nullable=True)
    absent_yn: Mapped[bool] = mapped_column(Boolean)
    absent_reason: Mapped[str] = mapped_column(String(STR_LEN_255))

    user: Mapped[User] = relationship("User")
    school: Mapped[School] = relationship("School")


class Exam(Base, TrackMixin):
    __tablename__ = "exam"

    exam_id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    exam_type: Mapped[str] = mapped_column(
        Enum(ExamType, length=STR_LEN_50, native_enum=False)
    )
    exam_name: Mapped[str] = mapped_column(String(STR_LEN_255))
    exam_description: Mapped[str] = mapped_column(String(STR_LEN_255), nullable=True)
    exam_date: Mapped[date] = mapped_column(Date, nullable=True)
    year: Mapped[int] = mapped_column(Integer, nullable=True)

    school: Mapped[School] = relationship("School")


class StudentExamMarks(Base, TrackMixin):
    __tablename__ = "student_exam_marks"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    exam_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(EXAM_ID_KEY_REF)
    )

    student_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    class_standard_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(CLASS_STANDARD_ID_KEY_REF)
    )
    subject_name: Mapped[str] = mapped_column(
        Enum(SubjectName, length=STR_LEN_50, native_enum=False)
    )
    marks_obtained: Mapped[str] = mapped_column(String(STR_LEN_255))
    marks_total: Mapped[str] = mapped_column(String(STR_LEN_255))
    theory_marks: Mapped[str] = mapped_column(String(STR_LEN_255))
    practical_marks: Mapped[str] = mapped_column(String(STR_LEN_255))
    assigned_teacher_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    invigilator_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    teachers_notes: Mapped[str] = mapped_column(String(STR_LEN_255))

    school = relationship("School", foreign_keys=[school_id])
    exam = relationship("Exam", foreign_keys=[exam_id])
    student = relationship("User", foreign_keys=[student_id])
    class_standard = relationship("ClassStandard", foreign_keys=[class_standard_id])
    assigned_teacher = relationship("User", foreign_keys=[assigned_teacher_id])
    invigilator = relationship("User", foreign_keys=[invigilator_id])


class ExamQuestionAssociation(Base, TrackMixin):
    __tablename__ = "exam_question_association"

    exam_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey("exam.exam_id"), primary_key=True
    )
    question_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey("questions_bank.question_id"), primary_key=True
    )


class QuestionBank(Base, TrackMixin):
    __tablename__ = "questions_bank"

    question_id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    class_standard_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(CLASS_STANDARD_ID_KEY_REF)
    )
    marks_weitage: Mapped[str] = mapped_column(String(STR_LEN_255))
    difficulty_level: Mapped[int] = mapped_column(Integer)
    question_statement: Mapped[str] = mapped_column(String(STR_LEN_255))
    question_description: Mapped[str] = mapped_column(String(STR_LEN_255))
    question_type: Mapped[str] = mapped_column(String(STR_LEN_255))

    option_one: Mapped[str] = mapped_column(String(STR_LEN_255))
    option_two: Mapped[str] = mapped_column(String(STR_LEN_255))
    option_three: Mapped[str] = mapped_column(String(STR_LEN_255))
    option_four: Mapped[str] = mapped_column(String(STR_LEN_255))
    option_five: Mapped[str] = mapped_column(String(STR_LEN_255))

    class_standard: Mapped[ClassStandard] = relationship(
        "ClassStandard", back_populates="question_bank"
    )
    responses: Mapped[List["StudentExamResponse"]] = relationship(
        "StudentExamResponse", back_populates="question"
    )


class StudentExamResponse(Base, TrackMixin):
    __tablename__ = "student_exam_response"

    id: Mapped[str] = mapped_column(String(STR_LEN_255), primary_key=True)
    school_id: Mapped[str] = mapped_column(
        String(STR_LEN_100), ForeignKey(SCHOOL_ID_KEY_REF), nullable=False
    )
    student_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(USER_ID_KEY_REF)
    )
    question_id: Mapped[str] = mapped_column(
        String(STR_LEN_255), ForeignKey(QUESTION_ID_KEY_REF)
    )
    student_response: Mapped[str] = mapped_column(String(STR_LEN_255))
    marks_assigned: Mapped[str] = mapped_column(String(STR_LEN_255))
    teachers_note: Mapped[str] = mapped_column(String(STR_LEN_255))

    school: Mapped[School] = relationship("School")
    student: Mapped[User] = relationship("User", back_populates="exam_answers")
    question: Mapped[QuestionBank] = relationship(
        "QuestionBank", back_populates="responses"
    )
