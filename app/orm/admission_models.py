from app.orm.db_connection import Base
from app.orm.core_models import TrackMixin


class StudentAdmissionForm(Base, TrackMixin):
    __tablename__ = "student_admission"


class TeacherHiringForm(Base, TrackMixin):
    __tablename__ = "teacher_enrollment"
