from typing import Optional
from pydantic import BaseModel, ConfigDict
from app.orm.core_models import UserType


class User(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    user_id: str
    first_name: str
    last_name: str
    email: str
    primary_contact_number: str


class BaseUserDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    user_id: str
    first_name: str
    last_name: str
    email: str
    user_type: UserType
    primary_contact_number: str


class LoginUserDTO(BaseModel):
    email: str
    password: str


class LoginResponseDTO(BaseModel):
    access_token: str
    user: BaseUserDTO


class CreateUserDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    first_name: str
    last_name: str
    email: str
    user_type: UserType
    primary_contact_number: str
    school_id: Optional[str] = None
    password: Optional[str] = None


class CreateSchoolDirector(BaseUserDTO):
    hashed_password: Optional[str] = None


class UpdateUserDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    first_name: Optional[str] = None
    last_name: Optional[str] = None
    email: Optional[str] = None
    school_id: Optional[str] = None
    primary_contact_number: Optional[str] = None


class LoggedInUser(BaseModel):
    sub: str  # Email
    sub_id: str  # UserId
    exp: int  # Expiry
