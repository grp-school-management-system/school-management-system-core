from pydantic import BaseModel
from typing import Optional


class StudentProfile(BaseModel):
    pass


class StaffProfile(BaseModel):
    pass


class SchoolProfile(BaseModel):
    pass
