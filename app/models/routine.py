from pydantic import BaseModel, ConfigDict
from typing import Optional, List
from app.models.address import BaseAddress
from app.orm.core_models import ClassPeriodName, SubjectName, WekkDayName
from datetime import time, date
from app.models.school import BaseSchool
from app.models.user_schemas import BaseUserDTO
from app.models.class_standard import BaseClassStandard


class BaseClassRoutine(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: str
    school_id: str
    class_id: str
    period: ClassPeriodName
    period_length: Optional[time]
    assigned_teacher_id: Optional[str]
    subject_name: Optional[SubjectName]
    week_day: Optional[WekkDayName]

    school: Optional[BaseSchool]
    assigned_teacher: Optional[BaseUserDTO]
    class_standard: Optional[BaseClassStandard]


class CreateClassRoutine(BaseModel):
    school_id: str
    class_id: str
    period: ClassPeriodName
    period_length: time = time.fromisoformat("01:30:00")
    assigned_teacher_id: Optional[str]
    subject_name: Optional[SubjectName]
    week_day: Optional[WekkDayName]


class UpdateClassRoutine(BaseModel):
    class_id: Optional[str]
    period: Optional[ClassPeriodName]
    period_length: Optional[time]
    assigned_teacher_id: Optional[str]
    subject_name: Optional[SubjectName]
    week_day: Optional[WekkDayName]


class BaseClassRoutineArchive(BaseModel):
    id: str
    school_id: str
    class_id: str
    period: Optional[ClassPeriodName]
    period_length: Optional[time]
    academic_date: Optional[date]

    assigned_teacher_id: Optional[str]
    subject_name: Optional[str]
    teacher_attended: Optional[bool]
    unavailability_reason: Optional[str]

    replaced_teacher_id: Optional[str]
    change_in_subject: Optional[str]
    replaced_subject_name: Optional[str]

    teacher_feedback: Optional[str]
    extra_details: Optional[str]


class CreateClassRoutineArchive(BaseModel):
    school_id: str
    class_id: str
    period: Optional[ClassPeriodName]
    period_length: Optional[time]
    academic_date: Optional[date]

    assigned_teacher_id: Optional[str]
    subject_name: Optional[str]
    teacher_attended: Optional[bool]
    unavailability_reason: Optional[str]

    replaced_teacher_id: Optional[str]
    change_in_subject: Optional[str]
    replaced_subject_name: Optional[str]

    teacher_feedback: Optional[str]
    extra_details: Optional[str]


class UpdateClassRoutineArchive(BaseModel):
    period: Optional[ClassPeriodName]
    period_length: Optional[time]
    academic_date: Optional[date]

    assigned_teacher_id: Optional[str]
    subject_name: Optional[str]
    teacher_attended: Optional[bool]
    unavailability_reason: Optional[str]

    replaced_teacher_id: Optional[str]
    change_in_subject: Optional[str]
    replaced_subject_name: Optional[str]

    teacher_feedback: Optional[str]
    extra_details: Optional[str]
