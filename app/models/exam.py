from pydantic import BaseModel, ConfigDict
from typing import Optional, List
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO
from datetime import date


class BaseExamDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    exam_id: str
    school_id: str
    exam_type: str
    exam_name: str
    exam_description: Optional[str]
    exam_date: Optional[date]
    year: Optional[int]


class CreateExamDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    exam_type: str
    exam_name: str
    exam_description: Optional[str]
    exam_date: Optional[date]
    year: Optional[int]


class UpdateExamDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    exam_type: str
    exam_name: str
    exam_description: Optional[str]
    exam_date: Optional[date]
    year: Optional[int]
