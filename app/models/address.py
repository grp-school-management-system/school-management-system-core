from pydantic import BaseModel, ConfigDict
from typing import Optional
from app.orm.core_models import AddressType


class BaseAddress(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: str
    location: str
    contact_number: str
    pin_code: str
    state: str
    address_type: AddressType


class CreateAddressDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    location: str
    contact_number: str
    pin_code: str
    state: str
    address_type: AddressType


class UpdateAddressDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    location: Optional[str]
    contact_number: Optional[str]
    pin_code: Optional[str]
    state: Optional[str]
    address_type: Optional[AddressType]
