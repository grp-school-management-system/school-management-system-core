from pydantic import BaseModel, ConfigDict
from app.orm.core_models import ClassStandardName
from typing import Optional, List
from app.models.school import BaseSchool
from app.models.user_schemas import BaseUserDTO


class BaseClassStandard(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    class_teacher_id: Optional[str]
    class_monitor_id: Optional[str]
    academic_year: int
    class_standard_name: ClassStandardName

    school: BaseSchool
    class_teacher: BaseUserDTO
    class_monitor: BaseUserDTO


class CreateClassStandardDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    class_teacher_id: Optional[str]
    class_monitor_id: Optional[str]
    academic_year: int
    class_standard_name: ClassStandardName


class UpdateClassStandardDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    class_teacher_id: Optional[str]
    class_monitor_id: Optional[str]
