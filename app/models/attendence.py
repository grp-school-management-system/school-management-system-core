from pydantic import BaseModel
from typing import Optional, List
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO
from datetime import date, time


class BaseAttendence(BaseModel):
    id: Optional[str]
    school_id: Optional[str]
    user_id: Optional[str]
    attendence_date: Optional[date]
    entry_time: Optional[time]
    exit_time: Optional[time]
    absent_yn: Optional[bool]
    absent_reason: Optional[str]


class CreateAttendence(BaseModel):
    school_id: Optional[str]
    user_id: Optional[str]
    attendence_date: Optional[date]
    entry_time: Optional[time]
    exit_time: Optional[time]
    absent_yn: Optional[bool]
    absent_reason: Optional[str]


class UpdateAttendence(BaseModel):
    school_id: str
    user_id: str
    attendence_date: Optional[date]
    entry_time: Optional[time]
    exit_time: Optional[time]
    absent_yn: Optional[bool]
    absent_reason: Optional[str]
