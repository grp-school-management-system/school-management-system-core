from pydantic import BaseModel
from typing import Optional, List
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO
from app.models.school import BaseSchool
from datetime import date


class BaseLeaves(BaseModel):
    id: str
    school_id: str
    user_id: str
    leave_description: str
    start_date: date
    end_date: date
    first_level_approved: bool
    first_level_approver_id: str
    second_level_approved: bool
    second_level_approved_by: str

    user: Optional[BaseUserDTO]
    school: Optional[BaseSchool]
    first_level_approver: Optional[BaseUserDTO]
    second_level_approver: Optional[BaseUserDTO]


class CreateLeavesDTO(BaseModel):
    school_id: str
    user_id: str
    leave_description: str
    start_date: date
    end_date: date
    first_level_approved: bool
    first_level_approver_id: str
    second_level_approved: bool
    second_level_approved_by: str


class UpdateLeavesDTO(BaseModel):
    leave_description: str
    start_date: date
    end_date: date
    first_level_approved: bool
    first_level_approver_id: str
    second_level_approved: bool
    second_level_approved_by: str
