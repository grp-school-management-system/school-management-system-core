from pydantic import BaseModel
from typing import Optional, List
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO


class BaseStudentExamResponse(BaseModel):
    id: str
    school_id: str
    enrollment_number: str
    question_id: str
    student_response: str
    marks_assigned: str
    teachers_note: str


class CreateStudentExamResponse(BaseModel):
    school_id: str
    enrollment_number: str
    question_id: str
    student_response: str
    marks_assigned: str
    teachers_note: str


class UpdateStudentExamResponse(BaseModel):
    marks_assigned: str
    teachers_note: str
