from pydantic import BaseModel, ConfigDict
from typing import Optional, List
from app.models.address import BaseAddress, CreateAddressDTO, UpdateAddressDTO
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO, CreateSchoolDirector
from app.orm.core_models import SchoolType


class BaseSchool(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    school_id: str
    name: str
    registration_number: str
    affiliation_number: str
    is_active: bool
    address: BaseAddress


class CreateSchoolDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    director: CreateSchoolDirector
    address: CreateAddressDTO
    registration_number: str
    affiliation_number: str
    is_active: bool
    school_type: SchoolType


class UpdateSchoolDTO(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: Optional[str] = None
    director: Optional[UpdateUserDTO] = None
    registration_number: Optional[str] = None
    affiliation_number: Optional[str] = None
    director: Optional[UpdateUserDTO] = None
    address: Optional[UpdateAddressDTO] = None
    is_active: Optional[bool] = None
