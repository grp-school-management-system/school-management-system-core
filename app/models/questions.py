from pydantic import BaseModel
from typing import Optional, List
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO


class BaseQuestion(BaseModel):
    question_id: str
    class_standard: str
    marks_weitage: str
    difficulty_level: int
    question_statement: str
    question_description: str
    question_type: str

    option_one: Optional[str]
    option_two: Optional[str]
    option_three: Optional[str]
    option_four: Optional[str]
    option_five: Optional[str]


class AddQuestionDTO(BaseModel):
    class_standard: str
    marks_weitage: str
    difficulty_level: int = 1
    question_statement: str
    question_description: Optional[str]
    question_type: str

    option_one: Optional[str]
    option_two: Optional[str]
    option_three: Optional[str]
    option_four: Optional[str]
    option_five: Optional[str]


class UpdateQuestionDTO(BaseModel):
    class_standard: Optional[str]
    marks_weitage: Optional[str]
    difficulty_level: Optional[int]
    question_statement: Optional[str]
    question_description: Optional[str]
    question_type: Optional[str]

    option_one: Optional[str]
    option_two: Optional[str]
    option_three: Optional[str]
    option_four: Optional[str]
    option_five: Optional[str]
