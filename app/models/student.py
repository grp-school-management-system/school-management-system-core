from pydantic import BaseModel
from typing import Optional, List
from datetime import datetime
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO


class BaseStudentDTO(BaseModel):
    student_id: str
    school_id: str
    enrollment_number: str
    current_class: str
    admission_year: datetime
    is_active: bool
    addresses: List[BaseAddress]


class CreateStudentDTO(BaseModel):
    user: BaseUserDTO
    current_class: str
    admission_year: datetime
    is_active: bool
    addresses: List[BaseAddress]


class UpdateStudentDTO(BaseModel):
    user: Optional[UpdateUserDTO]
    current_class: Optional[str]
    is_active: Optional[bool]
    addresses: List[BaseAddress]
