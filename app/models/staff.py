from pydantic import BaseModel
from typing import Optional, List
from app.models.address import BaseAddress
from app.models.user_schemas import BaseUserDTO, UpdateUserDTO


class BaseStaffDTO(BaseModel):
    staff_id: str
    school_id: str
    registration_number: str
    registration_year: str
    teaching_level: str
    is_active: bool

    addresses: List[BaseAddress]


class CreateStaffDTO(BaseModel):
    user: BaseUserDTO
    school_id: str
    registration_number: str
    registration_year: str
    teaching_level: str
    is_active: bool

    addresses: List[BaseAddress]


class UpdateStaffDTO(BaseModel):
    user: Optional[UpdateUserDTO]
    teaching_level: Optional[str]
    is_active: Optional[bool]

    addresses: List[BaseAddress]
