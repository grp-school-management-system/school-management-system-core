from kafka import KafkaConsumer
from pprint import pformat
from json import loads

consumer = KafkaConsumer(
    bootstrap_servers=["localhost:29092"],
    auto_offset_reset="earliest",
    enable_auto_commit=True,
    group_id="my-group",
    value_deserializer=lambda x: loads(x.decode("utf-8")),
)
consumer.subscribe(topics=["foobar"])
for msg in consumer:
    print(pformat(msg.value))
