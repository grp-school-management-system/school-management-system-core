from kafka import KafkaProducer
from time import sleep
from json import dumps

producer = KafkaProducer(
    bootstrap_servers=["localhost:29092"],
    value_serializer=lambda x: dumps(x).encode("utf-8"),
)

for _ in range(10):
    producer.send("foobar", b"This is toekn")
    sleep(5)
