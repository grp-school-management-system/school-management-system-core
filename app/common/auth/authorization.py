from typing import Annotated, Union
from fastapi import Header, Depends, Request
from loguru import logger
import jwt
from app.config.settings import Settings
from app.models.user_schemas import LoggedInUser
from loguru import logger
from app.common.auth.authentication import authentication

settings = Settings()


class Authorization:
    def __call__(
        self,
        request: Request,
        auth_key=Depends(authentication),
    ) -> LoggedInUser:
        if auth_key.find("Bearer ") != -1:
            auth_key = auth_key.removeprefix("Bearer ")
        logged_in_user = jwt.decode(
            auth_key, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        return LoggedInUser(**logged_in_user)


authorization = Authorization()
