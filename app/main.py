from pathlib import Path
from fastapi import FastAPI

import uvicorn
import threading
from loguru import logger
import time
import socketio

from app.api.v1.api import api_router
from app.api.websocket.websocket_router import websocket_router
from app.custom_logging import CustomizeLogger
from app.libs.rabbitmq.rabbitmq_test_util import test_rabbitmq
from app.utils.middlewares import ExceptionHandlerMiddleware
from fastapi.middleware.cors import CORSMiddleware

# config_path = Path(__file__).with_name("logging_config.json")

sio = socketio.AsyncServer(
    cors_allowed_origins="*", async_mode="asgi"
)  # , engineio_path="/ws")#wrap with ASGI application

origins = [
    "http://localhost",
    "http://localhost:8081",
]


def create_app() -> FastAPI:
    app = FastAPI(title="Demo Application")
    # logger = CustomizeLogger.make_logger(config_path)
    # app.logger = logger

    return app


app = create_app()
# app.include_router(websocket_router)
app.add_middleware(ExceptionHandlerMiddleware)
socket_app = socketio.ASGIApp(sio, socketio_path="socket.io")
sio.transports = ["websocket", "xhr-polling"]
sio.polling_duration = 10
app.include_router(api_router, prefix="/api/v1")

app.mount("", socket_app)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def start_application():
    uvicorn.run(app, host="0.0.0.0", port=8080)


t1 = threading.Thread(target=start_application)
t2 = threading.Thread(target=test_rabbitmq)

if __name__ == "__main__":
    t1.start()
    t2.start()
    # test_receiver()
    # start_application()
