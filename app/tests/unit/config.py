from app.main import app
from fastapi.testclient import TestClient
from unittest.mock import Mock
from app.orm.db_connection import get_db
from sqlalchemy import create_engine, StaticPool
from sqlalchemy.orm import sessionmaker
from app.orm.db_connection import Base

SQLALCHEMY_DATABASE_URL = "sqlite://"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
    # echo=True
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


Base.metadata.create_all(bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


client = TestClient(app)

app.dependency_overrides[get_db] = override_get_db
