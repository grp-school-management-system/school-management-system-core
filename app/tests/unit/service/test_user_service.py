# from app.orm.db_connection import get_db
# from app.models import user_schemas
# from app.service.user_service import create_user, get_user_by_id,add_users_bulk, search_user_by_fields_or, search_user_by_fields
# from pprint import pprint
# from .get_model_dummy_data import get_dummy_user_model_data, get_dummy_address_model_data
# from .get_schema_dummy_data import get_dummy_user_schema_data
# import pytest
# from loguru import logger
# import sys
# from uuid import uuid4
# from app.models.school import CreateSchoolDTO
# from app.models.user_schemas import CreateUserDTO
# from app.models.address import CreateAddressDTO
# from app.orm.core_models import School, User, Address, SchoolType, UserType

# sys.tracebacklimit = 0
# # def exception_handler(exception_type, exception, traceback):
# #   logger.info(f"{exception_type.__name__} , {exception}")

# # sys.excepthook = exception_handler
# class TestUserService:
#   db = next(get_db())

#   def clean_user_table(self):
#     self.db.query(Address).delete()
#     self.db.query(User).delete()
#     self.db.flush()
#     self.db.commit()


#   @pytest.fixture
#   def init_db(self, autouse=True):
#     self.clean_user_table()

#   def test_school_data(self):
#     school_id = str(uuid4())
#     school_director: CreateUserDTO = CreateUserDTO(
#       school_id=school_id,
#       first_name="first_name",
#       last_name="last_name",
#       email="email@gmail.com",
#       primary_contact_number="9192834675",
#       hashed_password="hased-password",
#       user_type=UserType.DIRECTOR
#     )
#     address: CreateAddressDTO = CreateAddressDTO(
#       location="location",
#       contact_number="9192939495",
#       pin_code="828384",
#       state="Bihar",
#       address_type="DEFAULT"
#     )
#     school_schema: CreateSchoolDTO = CreateSchoolDTO(
#       name="school-one",
#       director=school_director,
#       school_address=address,
#       registration_number="CND12FN341",
#       affiliation_number="FCX21DF23",
#       school_type=SchoolType.INDIVIDUAL,
#       is_active=True
#     )
#     school_address: Address = Address(id=str(uuid4()), **address.model_dump())
#     school_director: User = User(user_id=str(uuid4()), **school_director.model_dump())
#     new_school: School = School(school_id=school_id, **school_schema.model_dump(exclude=["director", "school_address"]))

#     new_school.address = school_address
#     self.db.add(new_school)
#     self.db.add(school_director)
#     self.db.commit()

#     added_school_director: School = self.db.query(User).filter(User.email == school_director.email).first()
#     assert added_school_director is not None
#   # def test_get_user_by_id(self):
#   #   try:
#   #     user_one: user_schemas.BaseUserDTO = get_dummy_user_schema_data()
#   #     user_two: user_schemas.BaseUserDTO  = get_dummy_user_schema_data()

#   #     created_user_one: user_schemas.User = create_user(db=self.db, user=user_one)
#   #     _: user_schemas.User = create_user(db=self.db, user=user_two)
#   #     res_user_one = get_user_by_id(db=self.db, id=created_user_one.user_id)
#   #     assert res_user_one != None and res_user_one.email == user_one.email and res_user_one.primary_contact_number == user_one.primary_contact_number
#   #   finally:
#   #     self.clean_user_table()

#   # def test_create_user(self):
#   #   try:
#   #     user: user_schemas.User = get_dummy_user_schema_data()
#   #     created_user: user_schemas.User = create_user(db=self.db, user=user)
#   #     result = get_user_by_id(db=self.db, id=created_user.user_id)
#   #     assert result.email == user.email and result.primary_contact_number == user.primary_contact_number
#   #   finally:
#   #     self.clean_user_table()

#   # def test_duplicate_user_creation(self):
#   #   try:
#   #     user: user_schemas.User = get_dummy_user_schema_data(121)
#   #     create_user(db=self.db, user=user)
#   #     create_user(db=self.db, user=user)
#   #     assert False
#   #   except Exception as ex:
#   #     logger.error(f"{repr(ex)}")
#   #   finally:
#   #     self.clean_user_table()

#   # def prepare_data_for_search_test(self):
#   #   user_one = get_dummy_user_schema_data()
#   #   user_two = get_dummy_user_schema_data()
#   #   user_three = get_dummy_user_schema_data()
#   #   user_four = get_dummy_user_schema_data()
#   #   add_users_bulk(db=self.db, users=[user_one, user_two, user_three, user_four])
#   #   return [user_one, user_two, user_three, user_four]

#   # def test_search_by_name(self):
#   #   try:
#   #     user_list = self.prepare_data_for_search_test()
#   #     by_name = dict(name=user_list[0].name)
#   #     result_by_name = search_user_by_fields(db=self.db, user=by_name)
#   #     assert len(result_by_name) == 1 and result_by_name[0].id == user_list[0].id
#   #   finally:
#   #     self.clean_user_table()

#   # def test_search_by_email(self):
#   #   try:
#   #     user_list = self.prepare_data_for_search_test()
#   #     by_email = dict(email=user_list[1].email)
#   #     result_by_email = search_user_by_fields(db=self.db, user=by_email)
#   #     assert len(result_by_email) == 1 and result_by_email[0].id == user_list[1].id
#   #   finally:
#   #     self.clean_user_table()

#   # def test_search_by_name_and_email(self):
#   #   try:
#   #     user_list = self.prepare_data_for_search_test()
#   #     by_name_and_email = SearchUserDTO(name=user_list[2].name, email=user_list[3].email)
#   #     result_by_name_and_email = search_user_by_fields_or(db=self.db, user=by_name_and_email)
#   #     assert len(result_by_name_and_email) == 2 and (user_list[2].name in [result_by_name_and_email[0].name, result_by_name_and_email[1].name])
#   #     assert len(result_by_name_and_email) == 2 and (user_list[3].email in [result_by_name_and_email[0].email, result_by_name_and_email[1].email])
#   #   finally:
#   #     self.clean_user_table()


# # class TestUserAddressRelation:
# #   db = next(get_db())

# #   def clean_user_table(self):
# #     self.db.query(Address).delete()
# #     self.db.query(User).delete()
# #     self.db.flush()
# #     self.db.commit()


# #   @pytest.fixture
# #   def init_db(self, autouse=True):
# #     self.clean_user_table()

# #   def test_get_user_addresses(self, init_db):
# #     try:
# #       dummy_user = get_dummy_user_model_data()
# #       address_one = get_dummy_address_model_data()
# #       address_two = get_dummy_address_model_data()
# #       dummy_user.addresses.append(address_one)
# #       dummy_user.addresses.append(address_two)
# #       self.db.add(dummy_user)
# #       self.db.flush()
# #       users = get_all_users(self.db)
# #       print(str(users))
# #       assert len(users) == 1 and len(users[0].addresses) == 2 and (address_one.id in [users[0].addresses[0].id, users[0].addresses[1].id])
# #       assert dummy_user.id == users[0].addresses[1].userId
# #       assert users[0] == users[0].addresses[1].user
# #     finally:
# #       self.clean_user_table()
