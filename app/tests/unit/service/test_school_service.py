# import pytest
# from app.models.school import CreateSchoolDTO, UpdateSchoolDTO, BaseSchool
# from app.models.user_schemas import CreateUserDTO, UpdateUserDTO
# from app.models.address import CreateAddressDTO
# from app.service.school_service import create_school, update_school
# from app.orm.db_connection import get_db
# from app.orm.core_models import Address, User, School
# from loguru import logger

# class TestSchoolService:
#   db = next(get_db())

#   def clean_user_table(self):
#     self.db.query(Address).delete()
#     self.db.query(User).delete()
#     self.db.query(School).delete()
#     self.db.flush()
#     self.db.commit()


#   @pytest.fixture
#   def init_db(self, autouse=True):
#     self.clean_user_table()

#   def test_create_school(self):
#     school_director: CreateUserDTO = CreateUserDTO(
#       first_name="first_name",
#       last_name="last_name",
#       email="email@gmail.com",
#       primary_contact_number="9192834675",
#       hashed_password="hased-password"
#     )
#     address: CreateAddressDTO = CreateAddressDTO(
#       location="location",
#       contact_number="9192939495",
#       pin_code="828384",
#       state="Bihar",
#       address_type="DEFAULT"
#     )
#     school_schema: CreateSchoolDTO = CreateSchoolDTO(
#       name="school-one",
#       director=school_director,
#       school_address=address,
#       registration_number="CND12FN341",
#       affiliation_number="FCX21DF23",
#       is_active=True
#     )
#     try:
#       new_school = create_school(self.db, school_schema)
#       assert new_school.affiliation_number == school_schema.affiliation_number and new_school.registration_number == school_schema.registration_number
#     except Exception as ex:
#       logger.error(f"not able to create school {repr(ex)}")
#       assert False

#   def test_update_school(self):
#     self.test_create_school()
#     update_user: UpdateUserDTO = UpdateUserDTO(
#       first_name="new_first",
#       last_name="new_last_name",
#       email="ew-email@gmail.com",
#       primary_contact_number="123122"
#     )
#     update_school_schema: UpdateSchoolDTO = UpdateSchoolDTO(
#       name="updated_school",
#       is_active=False,
#       director=update_user
#     )
#     try:
#       updated_school: BaseSchool = update_school(self.db, update_school_schema)
#       assert updated_school.is_active == update_school_schema.is_active and updated_school.name == update_school_schema.name
#       assert updated_school.director_name.email == update_school_schema.director.email
#     except Exception as ex:
#       logger.error(f"Error while updating school {repr(ex)}")
#       assert False
