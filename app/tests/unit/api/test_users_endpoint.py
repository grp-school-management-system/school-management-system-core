from unittest import TestCase
from unittest.mock import patch
from app.tests.unit.config import client
from app.models.user_schemas import CreateUserDTO, BaseUserDTO
from loguru import logger
from app.orm.core_models import UserType
from fastapi.encoders import jsonable_encoder
from uuid import uuid4


class TestUsersEndpoint(TestCase):
    @patch("app.service.user_service.create_user")
    def test_user_create_api(self, create_user):
        create_user.return_value = BaseUserDTO(
            user_id=str(uuid4()),
            first_name="Hritik",
            last_name="kumar",
            primary_contact_number="90",
            user_type=UserType.ADMIN,
            email="email",
        )
        request_payload: CreateUserDTO = CreateUserDTO(
            first_name="Hritik",
            last_name="kumar",
            primary_contact_number="90",
            user_type=UserType.ADMIN,
            email="email",
            school_id="abcd",
            hashed_password="abcd",
        )
        request_json = jsonable_encoder(request_payload)
        response = client.post("/api/v1/users/signup", json=request_json)
        logger.info(f"{response.json()}")
