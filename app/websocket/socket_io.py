from app.main import sio
from loguru import logger


@sio.on("connection")
async def on_connect(sid):
    logger.info(f"{sid}")


@sio.event
async def connect(sid, *args, **kwargs):
    print("connect ", sid)
    await sio.emit("msg", "Hello from FastAPI!")


@sio.event
async def disconnect(sid):
    print("disconnect ", sid)


@sio.event
async def connect_error(data):
    print("The connection failed!", data)


@sio.on("USER_CHAT_MESSAGE")
async def user_chat_message(sid, data):
    logger.info(f"sid: {sid} data: {data}")
    await sio.emit("msg", "Hello from FastAPI!")
