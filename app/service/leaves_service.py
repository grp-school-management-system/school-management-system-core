from sqlalchemy.orm import Session
from app.orm.core_models import Leaves, School, User
from app.models.leaves import BaseLeaves, CreateLeavesDTO, UpdateLeavesDTO
from typing import List
from app.utils.helper import copy_attributes, get_unique_id
from app.utils.exceptions import (
    UserNotFoundException,
    UserDoesNotBelongToSchoolException,
    NoDetailFoundException,
)


def validate_user_and_school_info(db: Session, user_id: str, school_id: str) -> None:
    user = db.query(User).filter(User.user_id == user_id).first()
    if user is None:
        raise UserNotFoundException()
    if user.school_id != school_id:
        raise UserDoesNotBelongToSchoolException()


def get_all_leaver_by_user_id(
    db: Session, school_id: str, user_id: str
) -> List[BaseLeaves]:
    return (
        db.query(Leaves)
        .filter(Leaves.school_id == school_id, Leaves.user_id == user_id)
        .all()
    )


def create_leave_for_user(db: Session, leave_payload: CreateLeavesDTO) -> BaseLeaves:
    validate_user_and_school_info(db, leave_payload.user_id, leave_payload.school_id)
    leave = Leaves(id=get_unique_id(), **leave_payload.model_dump())
    db.add(leave)
    db.commit()


def update_user_leaves(
    db: Session,
    school_id: str,
    user_id: str,
    leave_id: str,
    leave_payload: UpdateLeavesDTO,
) -> BaseLeaves:
    validate_user_and_school_info(db, user_id, school_id)
    leave = db.query(Leaves).filter(Leaves.id == leave_id).first()
    if leave is None:
        raise NoDetailFoundException()
    copy_attributes(leave, leave_payload)
    db.commit()
