from sqlalchemy.orm import Session
from app.models.question_response import (
    BaseStudentExamResponse,
    CreateStudentExamResponse,
    UpdateStudentExamResponse,
)
from app.orm.core_models import School, User, StudentExamResponse
from app.utils.helper import copy_attributes, get_unique_id
from typing import List
from app.utils.exceptions import (
    SchoolIdNotFoundException,
    UserNotFoundException,
    UserDoesNotBelongToSchoolException,
    StudentAnswerNotFoundException,
)


def validate_answer_payload(db: Session, answer: CreateStudentExamResponse) -> User:
    school = db.query(School).filter(answer.school_id).first()
    if school is None:
        raise SchoolIdNotFoundException()
    user = db.query(User).filter(User.user_id == answer.student_id).first()
    if user is None:
        raise UserNotFoundException()
    if user.school_id != school.school_id:
        raise UserDoesNotBelongToSchoolException()
    return user


def get_answer_by_id(db: Session, answer_id: str) -> BaseStudentExamResponse:
    return (
        db.query(StudentExamResponse)
        .filter(StudentExamResponse.id == answer_id)
        .first()
    )


def get_student_responses(
    db: Session, school_id: str, student_id: str
) -> List[BaseStudentExamResponse]:
    return (
        db.query(StudentExamResponse)
        .filter(
            StudentExamResponse.school_id == school_id,
            StudentExamResponse.student_id == student_id,
        )
        .all()
    )


def get_responses_for_question(db: Session, question_id: str) -> List[BaseException]:
    return (
        db.query(StudentExamResponse)
        .filter(StudentExamResponse.question_id == question_id)
        .all()
    )


def create_question_response(
    db: Session, answer: CreateStudentExamResponse
) -> BaseStudentExamResponse:
    validate_answer_payload(db, answer)
    student_response = StudentExamResponse(id=get_unique_id(), **answer.model_dump())
    db.add(student_response)
    db.commit()


def update_student_answer(
    db: Session, answer_id: str, answer: UpdateStudentExamResponse
) -> BaseStudentExamResponse:
    user = validate_answer_payload(db, answer)
    saved_answer = (
        db.query(StudentExamResponse)
        .filter(StudentExamResponse.id == answer_id)
        .first()
    )
    if saved_answer is None or user.user_id != saved_answer.student_id:
        raise StudentAnswerNotFoundException()
    copy_attributes(saved_answer, answer)
    db.commit()


def delete_student_answer(
    db: Session, school_id: str, student_id: str, answer_id: str
) -> bool:
    db.query(StudentExamResponse).filter(
        StudentExamResponse.id == answer_id,
        StudentExamResponse.school_id == school_id,
        StudentExamResponse.student_id == student_id,
    ).delete()
    db.commit()
