from sqlalchemy.orm import Session
from app.orm.core_models import ClassRoutine, ClassRoutineArchive
from typing import List
from app.models.routine import (
    BaseClassRoutine,
    BaseClassRoutineArchive,
    CreateClassRoutine,
    CreateClassRoutineArchive,
    UpdateClassRoutine,
    UpdateClassRoutineArchive,
)
from app.utils.helper import copy_attributes, get_unique_id

# Class Routine
def get_class_routine_by_id(
    db: Session, school_id: str, class_id: str, routine_id
) -> BaseClassRoutine:
    return (
        db.query(ClassRoutine)
        .filter(
            ClassRoutine.school_id == school_id,
            ClassRoutine.class_id == class_id,
            ClassRoutine.id == routine_id,
        )
        .first()
    )


def get_all_school_class_routine(
    db: Session, school_id: str, class_id: str
) -> List[BaseClassRoutine]:
    return (
        db.query(ClassRoutine)
        .filter(
            BaseClassRoutine.school_id == school_id, ClassRoutine.class_id == class_id
        )
        .all()
    )


def create_school_class_routine(
    db: Session, class_routine_req: CreateClassRoutine
) -> BaseClassRoutine:
    new_class_routine: ClassRoutine = ClassRoutine(
        class_id=get_unique_id(), **class_routine_req.model_dump()
    )
    db.add(new_class_routine)
    db.commit()


def update_school_class_routine(
    db: Session,
    school_id: str,
    class_id: str,
    class_routine_id: str,
    update_class_routine: UpdateClassRoutine,
) -> BaseClassRoutine:
    existing_class_routine = (
        db.query(ClassRoutine)
        .filter(
            ClassRoutine.school_id == school_id,
            ClassRoutine.class_id == class_id,
            ClassRoutine.id == class_routine_id,
        )
        .first()
    )
    copy_attributes(existing_class_routine, update_class_routine)
    db.commit()


# Class Routine Archive
def get_class_routine_archive_by_id(
    db: Session, school_id: str, class_id: str, routine_archive_id
) -> BaseClassRoutineArchive:
    return (
        db.query(ClassRoutineArchive)
        .filter(
            ClassRoutineArchive.school_id == school_id,
            ClassRoutineArchive.class_id == class_id,
            ClassRoutineArchive.id == routine_archive_id,
        )
        .first()
    )


def get_all_school_class_routine_archive(
    db: Session, school_id: str, class_id: str
) -> List[BaseClassRoutineArchive]:
    return (
        db.query(ClassRoutineArchive)
        .filter(
            BaseClassRoutineArchive.school_id == school_id,
            ClassRoutineArchive.class_id == class_id,
        )
        .all()
    )


def create_school_class_routine_archive(
    db: Session, class_routine_archive_req: CreateClassRoutineArchive
) -> BaseClassRoutineArchive:
    new_class_routine_archive: ClassRoutineArchive = ClassRoutineArchive(
        class_id=get_unique_id(), **class_routine_archive_req.model_dump()
    )
    db.add(new_class_routine_archive)
    db.commit()


def update_school_class_routine_archive(
    db: Session,
    school_id: str,
    class_id: str,
    class_routine_archive_id: str,
    update_class_routine_archive: UpdateClassRoutineArchive,
) -> BaseClassRoutineArchive:
    existing_class_routine_archive = (
        db.query(ClassRoutineArchive)
        .filter(
            ClassRoutineArchive.school_id == school_id,
            ClassRoutineArchive.class_id == class_id,
            ClassRoutineArchive.id == class_routine_archive_id,
        )
        .first()
    )
    copy_attributes(existing_class_routine_archive, update_class_routine_archive)
    db.commit()
