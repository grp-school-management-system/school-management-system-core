from sqlalchemy.orm import Session
from sqlalchemy import or_, and_
from typing import Any, Union, List, Annotated
from fastapi import Depends
from app.models import user_schemas
from app.orm.core_models import User, Address, School, UserType
from app.utils.validator import validate_email, validate_contact_number
from loguru import logger
from app.utils.helper import copy_attributes, get_unique_id
from app.utils.exceptions import (
    UserAlreadyExistException,
    SchoolIdNotProvidedException,
    InternalServerError,
    UserNotFoundException,
    SchoolIdDoesNotExistsException,
    PasswordNotMatchedException,
    InvalidTokenException,
)
from jwt.exceptions import InvalidTokenError
from app.config.settings import get_settings
from app.service import login_service
import jwt
from app.service.login_service import get_password_hash

settings = get_settings()


def validate_update_user_details(user: user_schemas.UpdateUserDTO):
    if user.email is not None:
        validate_email(user.email)
    if user.primary_contact_number is not None:
        validate_contact_number(user.primary_contact_number)


def validate_create_user_details(db: Session, user: user_schemas.CreateUserDTO):
    validate_email(user.email)
    validate_contact_number(user.primary_contact_number)
    user_info = db.query(User).filter(User.email == user.email).first()
    if user_info is not None:
        raise UserAlreadyExistException(
            f"User with mail id {user.email} already exists"
        )
    if user.school_id is None:
        raise SchoolIdNotProvidedException("School id for user is not provided")


def login_user(
    db: Session, user: user_schemas.LoginUserDTO
) -> user_schemas.LoginResponseDTO:
    user_info = db.query(User).filter(User.email == user.email).first()
    if user_info is None:
        raise UserNotFoundException()
    if not login_service.verify_password(user.password, user_info.hashed_password):
        raise PasswordNotMatchedException()
    access_token = login_service.create_access_token(
        {"sub": user_info.email, "sub_id": user_info.user_id}
    )
    token = user_schemas.LoginResponseDTO(access_token=access_token, user=user_info)
    return token


def get_current_user(
    logged_in_user: user_schemas.LoggedInUser, db: Session
) -> user_schemas.BaseUserDTO:
    try:
        email: str = logged_in_user.sub
        if email is None:
            raise InvalidTokenException()
    except InvalidTokenError:
        raise InvalidTokenException()
    user = get_user_by_email(db, email=email)
    if user is None:
        raise InvalidTokenException()
    return user


def get_user_by_email(db: Session, email: str) -> user_schemas.BaseUserDTO:
    return db.query(User).filter(User.email == email).first()


def get_users_by_ids(db: Session, user_id_list: List[str]) -> List[user_schemas.User]:
    result = db.query(User).filter(User.user_id.in_(user_id_list))
    return result


def get_user_by_id(db: Session, id: str) -> user_schemas.User:
    result = db.query(User).filter_by(user_id=id).first()
    return result


def create_user(
    db: Session, user_detail: user_schemas.CreateUserDTO
) -> user_schemas.BaseUserDTO:
    try:
        new_user_id = get_unique_id()
        validate_create_user_details(db, user_detail)
        new_user = User(
            user_id=new_user_id, **user_detail.model_dump(exclude=["password"])
        )
        new_user.hashed_password = get_password_hash(user_detail.password)
        db.add(new_user)
        db.commit()
        db.refresh(new_user)
    except Exception as ex:
        db.rollback()
        logger.error(f"Not able to create user: {ex}")
        raise InternalServerError(f"Could not create user: Detail {ex.args}")
    return new_user


def search_user_by_fields(db: Session, user: dict):
    result = []
    try:
        result = db.query(User).filter_by(**user).all()
        print(result)
    except Exception as ex:
        print("Field Search Failed: " + str(ex))
    return result


def search_user_by_fields_or(
    db: Session, user: user_schemas.User
) -> Union[user_schemas.User, None]:
    result = None
    try:
        query = or_(User.first_name == user.first_name, User.email == user.email)
        result = db.query(User).filter(query).first()
        result = (
            db.query(User)
            .filter(or_(User.first_name == user.first_name, User.email == user.email))
            .first()
        )
        print(result)
    except Exception as ex:
        print("OR Search Failed: " + str(ex))
    return result


def search_user_by_fields_and(db: Session, user: user_schemas.User):
    result = []
    try:
        result = (
            db.query(User)
            .filter_by(
                and_(User.first_name == user.first_name, User.email == user.email)
            )
            .all()
        )
        print(result)
    except Exception as ex:
        print("AND Search Failed: " + str(ex))
    return result


def update_user(
    db: Session, user_id: str, user: user_schemas.UpdateUserDTO
) -> user_schemas.BaseUserDTO:
    try:
        validate_update_user_details(user)
        user_info = db.query(User).filter(User.user_id == user_id).first()
        if user_info is None:
            raise UserNotFoundException("User does not exists")
        if user.email:
            user_email_val = db.query(User).filter(User.email == user.email).first()
            if (
                user_email_val is not None
                and user_info.user_id != user_email_val.user_id
            ):
                raise UserAlreadyExistException(
                    f"User with mail {user.email} already exists"
                )
        if user.school_id is not None:
            school = db.query(School).filter(School.school_id == user.school_id).first()
            if school is None:
                raise School("School id for user is not provided")
        copy_attributes(user_info, user)
        db.commit()
        db.refresh(user_info)
        return user_info
    except Exception:
        db.rollback()
        raise InternalServerError()


def update_school_director(
    db: Session, school_id: str, update_director: user_schemas.UpdateUserDTO
) -> user_schemas.BaseUserDTO:
    school_director = (
        db.query(User)
        .filter(User.school_id == school_id, User.user_type == UserType.DIRECTOR)
        .first()
    )
    if school_director is None:
        raise SchoolIdDoesNotExistsException()
    copy_attributes(school_director, update_director)
    db.commit()
    return school_director


def add_users_bulk(db: Session, users: List[user_schemas.CreateUserDTO]):
    users_models = []
    for user in users:
        validate_create_user_details(db, user)
        new_user_id = get_unique_id()
        users_models.append(User(user_id=new_user_id, **user.model_dump()))
    try:
        db.add_all(users_models)
    except Exception:
        db.rollback()
        return "Insertion Failed"
    else:
        db.commit()
    return "users added successfully"


def delete_user_by_id(db: Session, user_id: str):
    try:
        db.query(User).filter_by(User.user_id == user_id).delete()
    except Exception:
        db.rollback()
        return "Deletion Failed"
    else:
        db.commit()
    return "users deleted successfully"


def delete_all_users(db: Session):
    try:
        db.query(User).delete()
        db.flush()
    except Exception:
        db.rollback()
        return "Insertion Failed"
    finally:
        db.commit()
    return "users added successfully"
