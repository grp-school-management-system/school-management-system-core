from sqlalchemy.orm import Session
from app.models.attendence import (
    BaseAttendence,
    BaseAttendence,
    CreateAttendence,
    UpdateAttendence,
)
from app.orm.core_models import User, School, Attendence
from app.utils.helper import copy_attributes, get_unique_id
from datetime import date, time
from typing import List
from app.utils.exceptions import (
    SchoolIdDoesNotExistsException,
    UserNotFoundException,
    NoDetailFoundException,
)


def get_all_users_attendence(
    db: Session, school_id: str, user_id: str
) -> List[Attendence]:
    user_attendences = (
        db.query(Attendence)
        .filter(Attendence.school_id == school_id, Attendence.user_id == user_id)
        .all()
    )
    return user_attendences


def mark_attendence(db: Session, attendence: CreateAttendence) -> BaseAttendence:
    school_id = attendence.school_id
    user_id = attendence.user_id
    school = db.query(School).filter(School.school_id == school_id).first()
    if school is None:
        raise SchoolIdDoesNotExistsException()
    user = db.query(User).filter(User.user_id == user_id).first()
    if user is None or user.school_id != school_id:
        raise UserNotFoundException()
    attendence_model = Attendence(
        id=get_unique_id(),
        **attendence.model_dump(exclude=["attendence_date", "entry_time", "exit_time"])
    )
    attendence_model.attendence_date = date.fromisoformat(attendence.attendence_date)
    attendence_model.entry_time = date.fromisoformat(attendence.entry_time)
    attendence_model.exit_time = date.fromisoformat(attendence.exit_time)
    db.add(attendence_model)
    db.commit()


def update_attendenc(
    db: Session, attendence_id: str, attendence: UpdateAttendence
) -> BaseAttendence:
    school_id = attendence.school_id
    user_id = attendence.user_id
    saved_attendence = (
        db.query(Attendence)
        .filter(
            Attendence.id == attendence_id,
            Attendence.school_id == school_id,
            Attendence.user_id == user_id,
        )
        .first()
    )
    if saved_attendence is None:
        raise NoDetailFoundException()
    copy_attributes(saved_attendence, attendence)
    db.commit()


def delete_attendence(
    db: Session, school_id: str, user_id: str, attendence_id: str
) -> bool:
    db.query(Attendence).filter(
        Attendence.id == attendence_id,
        Attendence.school_id == school_id,
        Attendence.user_id == user_id,
    ).delete()
    db.commit()
