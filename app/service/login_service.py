from fastapi.security import OAuth2PasswordBearer
from datetime import timezone, datetime, timedelta
import jwt
import bcrypt
from app.config.settings import get_settings


settings = get_settings()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def verify_password(plain_password, hashed_password) -> bool:
    return bcrypt.checkpw(
        plain_password.encode("utf-8"), bytes.fromhex(hashed_password)
    )


def get_password_hash(password) -> str:
    return bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).hex()


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + timedelta(
            minutes=float(settings.ACCESS_TOKEN_EXPIRE_MINUTES)
        )
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM
    )
    return encoded_jwt
