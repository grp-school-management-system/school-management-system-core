from sqlalchemy.orm import Session
from app.models.questions import BaseQuestion, AddQuestionDTO, UpdateQuestionDTO
from app.utils.helper import copy_attributes, get_unique_id
from app.orm.core_models import QuestionBank
from typing import List
from app.utils.exceptions import QuestionDoesNotExistsException


def get_question_list(db: Session) -> List[BaseQuestion]:
    return db.query(QuestionBank).all()


def get_question_for_class_standard(
    db: Session, class_standard_id: str
) -> List[QuestionBank]:
    question = (
        db.query(QuestionBank)
        .filter(QuestionBank.class_standard_id == class_standard_id)
        .all()
    )
    if question is None:
        raise QuestionDoesNotExistsException()
    return question


def get_question_by_id(db: Session, question_id: str) -> BaseQuestion:
    question = (
        db.query(QuestionBank).filter(QuestionBank.question_id == question_id).first()
    )
    if question is None:
        raise QuestionDoesNotExistsException()
    return question


def add_question(db: Session, add_question: AddQuestionDTO) -> BaseQuestion:
    question = QuestionBank(question_id=get_unique_id(), **add_question.model_dump())
    db.add(question)


def update_question(
    db: Session, question_id: str, update_question: UpdateQuestionDTO
) -> BaseQuestion:
    question = (
        db.query(QuestionBank).filter(QuestionBank.question_id == question_id).first()
    )
    if question is None:
        raise QuestionDoesNotExistsException()
    copy_attributes(question, update_question)
    db.commit()


def delete_question(db: Session, question_id: str) -> bool:
    db.query(QuestionBank).filter(QuestionBank.question_id == question_id).delete()
    db.commit()
