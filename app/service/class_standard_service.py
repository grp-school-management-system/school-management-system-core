from sqlalchemy.orm import Session
from app.orm.core_models import ClassStandard
from typing import List
from app.models.class_standard import (
    BaseClassStandard,
    CreateClassStandardDTO,
    UpdateClassStandardDTO,
)
from app.utils.helper import copy_attributes, get_unique_id


def get_class_standard_by_id(
    db: Session, school_id: str, class_standard_id: str
) -> BaseClassStandard:
    return (
        db.query(ClassStandard)
        .filter(
            ClassStandard.school_id == school_id,
            ClassStandard.class_id == class_standard_id,
        )
        .first()
    )


def get_all_school_class_standard(
    db: Session, school_id: str
) -> List[BaseClassStandard]:
    return db.query(ClassStandard).filter(ClassStandard.school_id == school_id).all()


def create_school_class_standard(
    db: Session, class_standard_req: CreateClassStandardDTO
) -> BaseClassStandard:
    new_class_standard: ClassStandard = ClassStandard(
        class_id=get_unique_id(), **class_standard_req.model_dump()
    )
    db.add(new_class_standard)
    db.commit()


def update_school_class_standard(
    db: Session,
    school_id: str,
    class_standard_id: str,
    update_class_standard: UpdateClassStandardDTO,
) -> BaseClassStandard:
    existing_class_standard = (
        db.query(ClassStandard)
        .filter(
            ClassStandard.school_id == school_id,
            ClassStandard.class_id == class_standard_id,
        )
        .first()
    )
    copy_attributes(existing_class_standard, update_class_standard)
    db.commit()
