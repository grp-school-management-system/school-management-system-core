from typing import List, Any
from app.models.school import BaseSchool, CreateSchoolDTO, UpdateSchoolDTO
from app.models.user_schemas import CreateUserDTO, BaseUserDTO
from sqlalchemy.orm import Session
from app.orm.core_models import School, Address
from loguru import logger
from app.utils.helper import copy_attributes, get_unique_id
from app.service.user_service import create_user, update_school_director
from app.utils.exceptions import SchoolIdDoesNotExistsException


def get_school_info_by_id(db: Session, school_id: str) -> BaseSchool:
    school_info = db.query(School).filter(School.school_id == school_id).first()
    if school_info is None:
        raise SchoolIdDoesNotExistsException(
            f"No School found for given school id {school_id}"
        )
    return school_info


def get_all_school_list(db: Session) -> List[BaseSchool]:
    return db.query(School).all()


def get_all_school_users(db: Session, school_id: str) -> List[BaseUserDTO]:
    school = db.query(School).filter(School.school_id == school_id).first()
    return school.users


def create_school(db: Session, school: CreateSchoolDTO) -> BaseSchool:
    try:
        new_school_id = get_unique_id()
        new_address_id = get_unique_id()
        new_school_entry = School(
            school_id=new_school_id,
            **school.model_dump(exclude=["director", "address"]),
        )
        school_address = Address(id=new_address_id, **school.address.model_dump())
        new_school_entry.address = school_address
        db.add(new_school_entry)
        school_director: CreateUserDTO = CreateUserDTO(
            school_id=new_school_id, **school.director.model_dump()
        )
        create_user(db, school_director)
        db.refresh(new_school_entry)
        return new_school_entry
    except Exception as ex:
        logger.exception(f"error while create school {repr(ex)}")


def update_school_info(
    db: Session, school_id: str, update_school_schema: UpdateSchoolDTO
) -> School:
    school_info = db.query(School).filter(School.school_id == school_id).first()
    if school_info is None:
        raise SchoolIdDoesNotExistsException(
            f"Could not find any school with id: {school_id}"
        )
    if update_school_schema.director:
        update_school_director(db, school_id, update_school_schema.director)
    if update_school_schema.address is not None:
        copy_attributes(
            school_info.address, {**update_school_schema.address.model_dump()}
        )
    copy_attributes(
        school_info,
        {**update_school_schema.model_dump(exclude=["director", "address"])},
    )

    db.commit()
    db.refresh(school_info)
    return school_info


def delete_school_by_id(db: Session, school_id: str) -> str:
    try:
        db.query(School).filter(School.school_id == school_id).delete()
    except Exception:
        db.rollback()
        raise SchoolIdDoesNotExistsException()
    else:
        db.commit()
    return "Successfully deleted"
