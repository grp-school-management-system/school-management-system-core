from pydantic_settings import BaseSettings, SettingsConfigDict
from functools import lru_cache


class Settings(BaseSettings):
    REDIS_HOST: str = ""
    REDIS_PASSWORD: str = ""
    REDIS_PORT: str = ""

    DATABASE_URL: str = ""

    RABBITMQ_USERNAME: str = ""
    RABBITMQ_PASSWORD: str = ""
    RABBITMQ_HOST: str = ""
    RABBITMQ_PORT: str = ""

    SECRET_KEY: str = ""
    ALGORITHM: str = ""
    ACCESS_TOKEN_EXPIRE_MINUTES: str = ""

    model_config = SettingsConfigDict(env_file=".env")


@lru_cache
def get_settings():
    settings = Settings()
    return settings
