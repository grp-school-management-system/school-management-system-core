from enum import Enum, auto
from typing import List, Dict, Any, Optional
from typing_extensions import override


class ErrorCode(Enum):
    BAD_REQUEST = auto()
    REQUEST_VARIABLE_MISSING = auto()
    REQUEST_VARIABLE_INVALID = auto()
    UNAUTHORIZED = auto()
    STREAM_DOES_NOT_EXIST = auto()


class JsonableError(Exception):
    code: ErrorCode = ErrorCode.BAD_REQUEST

    data_fields: List[str] = []
    http_status_code: int = 400

    def __init__(self, msg: str) -> None:
        self.msg = msg

    @staticmethod
    def msg_format() -> str:
        return "{_msg}"

    @property
    def extra_headers(self) -> Dict[str, Any]:
        return {}

    @property
    def msg(self) -> str:
        format_data = dict(
            ((f, getattr(self, f)) for f in self.data_fields),
            _msg=getattr(self, "_msg", None),
        )
        return self.msg_format().format(**format_data)

    @property
    def data(self) -> Dict[str, Any]:
        return dict(
            ((f, getattr(self, f)) for f in self.data_fields), code=self.code.name
        )

    @override
    def __str__(self) -> str:
        return self.msg


class UnauthorizedError(JsonableError):
    code: ErrorCode = ErrorCode.UNAUTHORIZED
    http_status_code: int = 401

    def __init__(
        self, msg: Optional[str] = None, www_authenticate: Optional[str] = None
    ) -> None:
        if msg is None:
            msg = _("Not logged in: API authentication or user session required")
        super().__init__(msg)
        if www_authenticate is None:
            self.www_authenticate = 'Basic realm="zulip"'
        elif www_authenticate == "session":
            self.www_authenticate = 'Session realm="zulip"'
        else:
            raise AssertionError("Invalid www_authenticate value!")

    @property
    @override
    def extra_headers(self) -> Dict[str, Any]:
        extra_headers_dict = super().extra_headers
        extra_headers_dict["WWW-Authenticate"] = self.www_authenticate
        return extra_headers_dict


class StreamDoesNotExistError(JsonableError):
    code = ErrorCode.STREAM_DOES_NOT_EXIST
    data_fields = ["stream"]

    def __init__(self, stream: str) -> None:
        self.stream = stream

    @staticmethod
    @override
    def msg_format() -> str:
        return _("Stream '{stream}' does not exist")
