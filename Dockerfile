FROM python:3.11

WORKDIR /usr/src

COPY ./requirements.txt ./requirements.txt
RUN pip3 install --no-cache-dir --upgrade -r requirements.txt

COPY ./app ./app
COPY ./alembic ./alembic
COPY ./alembic.ini .

CMD ["uvicorn", "app.main:app", "--port", "8080", "--host", "0.0.0.0"]
